import org.junit.Assert;
import org.junit.Test;
import ru.zakharova.Calculation;

public class Testing {

    //Calculating an expression with natural numbers
    @Test
    public void test_1() {
        Assert.assertEquals(3.0, Calculation.calculate(7, 2, 2, 1), 1);
    }

    //Calculating an expression with negative numbers
    @Test
    public void test_2() {
        Assert.assertEquals(1.0, Calculation.calculate(-1, -2, -3, 0), 1);
    }

    //Calculating an expression with positive real numbers
    @Test
    public void test_3() {
        Assert.assertEquals(1.0, Calculation.calculate(1.5, 2.5, 0.5, 3.5), 1);
    }

    //Calculating an expression with negative real numbers
    @Test
    public void test_4() {
        Assert.assertEquals(-1.0, Calculation.calculate(-1.5, -2.5, 0.5, 3.5), 1);
    }

    //Division by zero
    @Test
    public void test_5() {
        Assert.assertTrue(Double.isInfinite(Calculation.calculate(1, 2, 0, 0)));
    }

    //Calculating an expression 0/0
    @Test
    public void test_6() {
        Assert.assertTrue(Double.isNaN(Calculation.calculate(-1, 1, 0, 0)));
    }

    //Check the validity of the data (correct data)
    @Test
    public void test_7() {
        String[] vars =  new String[] {"1", "1", "1", "1"};
        Assert.assertTrue(Calculation.validate(vars));
    }

    //Check the validity of the data (not enough data)
    @Test
    public void test_8() {
        String[] vars =  new String[] {"1", "1", "1"};
        Assert.assertFalse(Calculation.validate(vars));
    }

    //Check the validity of the data (received a character, not a number)
    @Test
    public void test_9() {
        String[] vars =  new String[] {"1", "1", "1", "r"};
        Assert.assertFalse(Calculation.validate(vars));
    }

    //Check the validity of the data (too much data)
    @Test
    public void test_10() {
        String[] vars =  new String[] {"1", "1", "1", "1", "1"};
        Assert.assertFalse(Calculation.validate(vars));
    }
}