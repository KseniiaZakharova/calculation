package ru.zakharova;

public class Calculation {

    public static double calculate(double a, double b, double c, double d) throws IllegalArgumentException {
        if (c + d == 0) {
            System.out.println("Division by zero");
        }
        return (a + b) / (c + d);
    }

    public static boolean validate(String[] vars) {
        if (vars.length != 4) {
            return false;
        }
        for (String elem : vars) {
            try {
                Double.parseDouble(elem);
            } catch (NumberFormatException e) {
                System.out.println("Wrong number in string");
                return false;
            }
        }

        return true;
    }

}
