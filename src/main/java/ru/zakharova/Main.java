package ru.zakharova;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the number a, b, c, d to calculate the function A(a, b, c, d) = (a + b) / (c + d)");
        System.out.println("Empty string or 'exit' close the program");
        String str = in.nextLine();
        while (!str.isEmpty() && !str.equals("exit")) {
            String[] vars = str.split(" ");
            if (!Calculation.validate(vars)) {
                System.out.println("Incorrect data was written");
                str = in.nextLine();
                continue;
            }

            System.out.println("Calucalted = " + Calculation.calculate(
                    Double.parseDouble(vars[0]),
                    Double.parseDouble(vars[1]),
                    Double.parseDouble(vars[2]),
                    Double.parseDouble(vars[3]))
            );
            str = in.nextLine();
        }

    }
}