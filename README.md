## Calculation
An application that calculates the expression A (a, b, c, d) = (a + b) / (c + d).  
**Input data:** four real numbers a, b, c, d.  
**Output data:** calculation result.  
**Developer:** Zakharova Ksenia

**How does it work?**  
1. To use the application, download the file calculation-1.0.jar to your computer. Enter at the command line
"java -jar calculation-1.0.jar".
2. To use the whole project:  
2.1 Press the Fork button (top right the page) to save copy of this project on your account.  
2.2 Download the repository files (project) from the download section or clone this project by typing in the bash the following command:  
git clone https: //gitlab.com/KseniiaZakharova/calculation.git  
2.3 Imported it in Intellij IDEA or any other Java IDE.  


